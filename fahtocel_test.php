<?php  
require_once(dirname(__FILE__) . '/simpletest/autorun.php');  
require_once('../classes/fahtocel.php');  
  
class TestFahToCel extends UnitTestCase { 
    function testConvert1()  
    {  
		$fahtocel = new fahtocel();
		$this->assertEqual(5, $fahtocel->convert(41));
    }  
    function testConvert2()  
    {  
		$fahtocel = new fahtocel();
		$this->assertTrue(5 == $fahtocel->convert(41));
		$this->assertFalse($fahtocel->convert(50) <> $fahtocel->convert(50));
		$this->assertIsA($fahtocel->convert(90), 'float');
		$this->assertEqual(-300, $fahtocel->convert(-508));
		$this->assertNotNull($fahtocel->convert(41));
    }  
}  
?>